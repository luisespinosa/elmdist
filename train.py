import glob
from gensim.models import Word2Vec
import json
from nltk.tokenize import word_tokenize
import logging
if __name__ == '__main__':

    # entity categories: Artist, Track, Album, Label
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    count=0
    listing = glob.glob("elmd2/*.json")
    out=set()
    lemmas_entities=dict()
    for infile in listing:
        #print('Processing:',infile)
        count+=1
        #print 'Processing: ',infile,' | Number ',count
        obj = json.load(open(infile))
        sents = sorted(obj, key=lambda x:x['index'])
        for s in sents:
            sent_text=s['text']
            new_sent = sent_text
            if s['entities']:
                for ent in s['entities']:
                    mbid = ent['mbid']
                    label = ent['label']
                    if ent['category'] != 'Artist' and ent['category'] != 'Album':
                        print ent
                    if len(label)>1:
                        if mbid != '':
                            if mbid in lemmas_entities:
                                label = lemmas_entities[mbid]
                            else:
                                label = label.lower().replace(" ","_")+'_mbid_'+ent['category']+'_'+mbid                              
                                lemmas_entities[mbid] = label
                        else:
                            label = label.lower().replace(" ","_")
                        to_replace = sent_text[ent['startChar']:ent['endChar']]
                        new_sent = new_sent.replace(to_replace,label)

            tokens=tuple(word_tokenize(new_sent.lower()))
            out.add(tokens)
    print len(lemmas_entities)
    model = Word2Vec(out,min_count=3, size=300, window=5, hs=1)
    model.save('elmd2_mbid')
    outf = open('mbids2label.json', 'w')
    json.dump(lemmas_entities,outf)