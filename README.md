# README #

Data associated with the SemDeep 2017 submission "ELMDist: A vector space model with words and MusicBrainz entities", collocated Workshop in ESWC 2017.

The pretrained sense-level word2vec model against MusicBrainz can be downloaded from:

http://mtg.upf.edu/system/files/projectsweb/elmdist_vectors.zip

After downloading all files, load the model e.g. with gensim(*) with:


```
#!python

import gensim

elmd_model = gensim.models.Word2Vec.load("elmd2_mbid")
```


(*) https://radimrehurek.com/gensim/models/word2vec.html


If you want to retrain the vectors, ELMD 2.0 can be downloaded from here:

http://mtg.upf.edu/download/datasets/elmd


And you can train the model runing train_word2vec.py


Please cite the following paper if using ELMDist:

Espinosa-Anke, L., Oramas S., Saggion H., & Serra X. (2017).  ELMDist: A vector space model with words and MusicBrainz entities. Workshop on Semantic Deep Learning (SemDeep), collocated with ESWC 2017

Paper: http://mtg.upf.edu/node/3761